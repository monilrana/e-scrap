-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 05, 2019 at 01:46 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `escrap`
--
CREATE DATABASE IF NOT EXISTS `escrap` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `escrap`;

-- --------------------------------------------------------

--
-- Table structure for table `accounts_user_registration`
--

CREATE TABLE IF NOT EXISTS `accounts_user_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` longtext NOT NULL,
  `city` varchar(255) NOT NULL,
  `mobile_no` varchar(10) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `license_img` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `accounts_user_registration`
--

INSERT INTO `accounts_user_registration` (`id`, `name`, `address`, `city`, `mobile_no`, `email_id`, `pwd`, `user_type`, `status`, `license_img`) VALUES
(2, 'satyam', 'valsad', 'valsad', '9087654321', 'satyam@yahoo.com', '111111', 'DEALER', 1, 'pics/d1.jpg'),
(3, 'monil', 'valsad', 'valsad', '9087654321', 'monil@yahoo.com', '111111', 'DEALER', 1, 'pics/d1.jpg'),
(4, 'welspun', 'vapi', 'vapi', '9087654321', 'welspun@yahoo.com', '111111', 'COMPANY', 1, 'pics/c1.png'),
(5, 'alok', 'vapi', 'vapi', '9087654321', 'alok@yahoo.com', '111111', 'COMPANY', 0, 'pics/c1.png');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add user_ registration', 7, 'add_user_registration'),
(26, 'Can change user_ registration', 7, 'change_user_registration'),
(27, 'Can delete user_ registration', 7, 'delete_user_registration'),
(28, 'Can view user_ registration', 7, 'view_user_registration'),
(29, 'Can add scrap_detail', 8, 'add_scrap_detail'),
(30, 'Can change scrap_detail', 8, 'change_scrap_detail'),
(31, 'Can delete scrap_detail', 8, 'delete_scrap_detail'),
(32, 'Can view scrap_detail', 8, 'view_scrap_detail'),
(33, 'Can add bid_detail', 9, 'add_bid_detail'),
(34, 'Can change bid_detail', 9, 'change_bid_detail'),
(35, 'Can delete bid_detail', 9, 'delete_bid_detail'),
(36, 'Can view bid_detail', 9, 'view_bid_detail'),
(37, 'Can add winner_detail', 10, 'add_winner_detail'),
(38, 'Can change winner_detail', 10, 'change_winner_detail'),
(39, 'Can delete winner_detail', 10, 'delete_winner_detail'),
(40, 'Can view winner_detail', 10, 'view_winner_detail'),
(41, 'Can add complain_ detail', 11, 'add_complain_detail'),
(42, 'Can change complain_ detail', 11, 'change_complain_detail'),
(43, 'Can delete complain_ detail', 11, 'delete_complain_detail'),
(44, 'Can view complain_ detail', 11, 'view_complain_detail');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$120000$aPMCrbDxI6On$QXIqPH5+itZ2CYO2+QoAuAYSYvXfwp1JDc09VUY85dw=', '2019-08-12 08:39:03.482134', 1, 'admin', 'satyam', 'satyam', 'admin@yahoo.com', 1, 1, '2019-08-09 11:30:27.000000'),
(2, 'pbkdf2_sha256$120000$gGyzLokSAbHU$nf/ayjJNja6CCWQ7o+d5Bls4iofLXHto1Ke7WIJ5Ewc=', NULL, 0, 'satyamadmin', 'satyam', '', 'satyamadmin@yahoo.com', 1, 1, '2019-08-12 08:38:06.000000');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bid_detail_bid_detail`
--

CREATE TABLE IF NOT EXISTS `bid_detail_bid_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bid_date` date NOT NULL,
  `d_regis_id` int(11) NOT NULL,
  `scrap_id` int(11) NOT NULL,
  `bid_amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bid_detail_bid_detail`
--

INSERT INTO `bid_detail_bid_detail` (`id`, `bid_date`, `d_regis_id`, `scrap_id`, `bid_amount`) VALUES
(1, '2019-09-21', 3, 1, 31000),
(2, '2019-09-22', 2, 1, 37000),
(3, '2019-09-23', 2, 2, 41000);

-- --------------------------------------------------------

--
-- Table structure for table `bid_detail_winner_detail`
--

CREATE TABLE IF NOT EXISTS `bid_detail_winner_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `winner_date` date NOT NULL,
  `d_regis_id` int(11) NOT NULL,
  `scrap_id` int(11) NOT NULL,
  `bid_id` int(11) NOT NULL,
  `bid_amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bid_detail_winner_detail`
--

INSERT INTO `bid_detail_winner_detail` (`id`, `winner_date`, `d_regis_id`, `scrap_id`, `bid_id`, `bid_amount`) VALUES
(1, '2019-09-23', 2, 1, 2, 37000);

-- --------------------------------------------------------

--
-- Table structure for table `complain_complain_detail`
--

CREATE TABLE IF NOT EXISTS `complain_complain_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `complain_date` date NOT NULL,
  `complainer_regis_id` int(11) NOT NULL,
  `complain_against_name` varchar(255) NOT NULL,
  `complain_reason` longtext NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `complain_complain_detail`
--

INSERT INTO `complain_complain_detail` (`id`, `complain_date`, `complainer_regis_id`, `complain_against_name`, `complain_reason`, `status`) VALUES
(1, '2019-09-24', 2, 'welspun', 'material uploaded was not good', 1),
(2, '2019-09-24', 4, 'satyam', 'not pay the payment at perfect time', 2);

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2019-08-09 11:32:02.070425', '1', 'admin', 2, '[{"changed": {"fields": ["first_name", "last_name"]}}]', 4, 1),
(2, '2019-08-12 08:38:06.199656', '2', 'satyamadmin', 1, '[{"added": {}}]', 4, 1),
(3, '2019-08-12 08:38:20.074776', '2', 'satyamadmin', 2, '[{"changed": {"fields": ["first_name"]}}]', 4, 1),
(4, '2019-08-12 08:38:41.867239', '2', 'satyamadmin', 2, '[{"changed": {"fields": ["is_staff"]}}]', 4, 1),
(5, '2019-08-12 08:39:18.122640', '2', 'satyamadmin', 2, '[{"changed": {"fields": ["email"]}}]', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(7, 'accounts', 'user_registration'),
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(9, 'bid_detail', 'bid_detail'),
(10, 'bid_detail', 'winner_detail'),
(11, 'complain', 'complain_detail'),
(5, 'contenttypes', 'contenttype'),
(8, 'scrap_detail', 'scrap_detail'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-08-09 11:23:23.150400'),
(2, 'auth', '0001_initial', '2019-08-09 11:23:41.663130'),
(3, 'admin', '0001_initial', '2019-08-09 11:23:45.414862'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-08-09 11:23:45.479322'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2019-08-09 11:23:45.557569'),
(6, 'contenttypes', '0002_remove_content_type_name', '2019-08-09 11:23:47.785163'),
(7, 'auth', '0002_alter_permission_name_max_length', '2019-08-09 11:23:49.949830'),
(8, 'auth', '0003_alter_user_email_max_length', '2019-08-09 11:23:51.272091'),
(9, 'auth', '0004_alter_user_username_opts', '2019-08-09 11:23:51.366465'),
(10, 'auth', '0005_alter_user_last_login_null', '2019-08-09 11:23:52.574025'),
(11, 'auth', '0006_require_contenttypes_0002', '2019-08-09 11:23:52.673368'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2019-08-09 11:23:52.772215'),
(13, 'auth', '0008_alter_user_username_max_length', '2019-08-09 11:23:54.150526'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2019-08-09 11:23:55.361376'),
(15, 'sessions', '0001_initial', '2019-08-09 11:23:56.963729'),
(16, 'accounts', '0001_initial', '2019-08-09 12:29:07.497278'),
(17, 'accounts', '0002_auto_20190809_1802', '2019-08-09 12:32:57.484170'),
(18, 'scrap_detail', '0001_initial', '2019-08-13 05:13:50.369272'),
(19, 'scrap_detail', '0002_auto_20190813_1211', '2019-08-13 06:41:53.394403'),
(20, 'bid_detail', '0001_initial', '2019-08-22 12:52:24.041996'),
(21, 'bid_detail', '0002_winner_detail', '2019-09-23 07:03:04.919868'),
(22, 'complain', '0001_initial', '2019-09-24 11:34:30.222596');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('06ct7vji4x1dsljctkxqtsxwd1zq763s', 'N2ViODRmMTcyMThmYWFkNTM3ZTljYTQwZjJlOWIzYzU1NzAwZDFkMTp7ImRlYWxlcl9pZCI6MiwiY29tcF9pZCI6NH0=', '2019-10-13 04:51:34.960712'),
('3yc492i552plzlbszq85xeqsse1wovc8', 'N2ViODRmMTcyMThmYWFkNTM3ZTljYTQwZjJlOWIzYzU1NzAwZDFkMTp7ImRlYWxlcl9pZCI6MiwiY29tcF9pZCI6NH0=', '2019-09-05 13:10:04.747081'),
('5277b7yff8g9mkv2wnatnh9nyuz5s4z9', 'N2ViODRmMTcyMThmYWFkNTM3ZTljYTQwZjJlOWIzYzU1NzAwZDFkMTp7ImRlYWxlcl9pZCI6MiwiY29tcF9pZCI6NH0=', '2019-10-08 12:20:54.508000'),
('y4r2ocqs5fl3210ob30764vk5jlu4mfc', 'NTNhOTNhMTM1MGE1MTgxMzI5YTliZjI3MTBjZGZiNDk2NGYzMDM0YTp7ImRlYWxlcl9pZCI6MywiY29tcF9pZCI6NH0=', '2019-10-07 12:26:46.667085');

-- --------------------------------------------------------

--
-- Table structure for table `scrap_detail_scrap_detail`
--

CREATE TABLE IF NOT EXISTS `scrap_detail_scrap_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scrap_name` varchar(255) NOT NULL,
  `scrap_desc` longtext NOT NULL,
  `city` varchar(255) NOT NULL,
  `c_regis_id` int(11) NOT NULL,
  `uom` varchar(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `minimum_price` int(11) NOT NULL,
  `scrap_upload_date` date NOT NULL,
  `last_bid_date` date NOT NULL,
  `scrap_img` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `scrap_detail_scrap_detail`
--

INSERT INTO `scrap_detail_scrap_detail` (`id`, `scrap_name`, `scrap_desc`, `city`, `c_regis_id`, `uom`, `qty`, `minimum_price`, `scrap_upload_date`, `last_bid_date`, `scrap_img`, `status`) VALUES
(1, 'paper', 'newspaper with 1000kg', 'valsad', 4, 'KG', 1000, 30000, '2019-09-12', '2019-09-22', 'pics/paper1_3VZ9gyt.jpg', 3),
(2, 'Plastic', 'Plastic Bottles with 1 ton', 'vapi', 4, 'TON', 1, 40000, '2019-09-21', '2019-10-01', 'pics/plastic1.jpg', 1),
(3, 'Copper', 'Copper with 2000 kg', 'vapi', 4, 'KG', 2000, 50000, '2019-09-22', '2019-10-02', 'pics/cooper1.jpg', 0),
(4, 'Rubber', 'rubber from tyre with 1000 kg', 'vapi', 4, 'KG', 1000, 50000, '2019-09-23', '2019-10-03', 'pics/rubber1.jpg', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
