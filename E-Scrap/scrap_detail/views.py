from django.shortcuts import render, redirect
from .models import Scrap_detail
from accounts.models import User_Registration
from bid_detail.models import Bid_detail
from datetime import datetime, timedelta
from django.contrib import messages
from datetime import date
from django.conf import settings
# Create your views here.

def company_upload_scrap(request):
    if request.method == 'POST' and request.FILES['txtimg']:
        sname = request.POST['txtname']
        sdesc = request.POST['txtdesc']
        city = request.POST['txtcity']
        uom = request.POST['seluom']
        qty = request.POST['txtqty']
        mprice = request.POST['txtmprice']
        simg = request.FILES['txtimg']

        scrap = Scrap_detail(scrap_name = sname,scrap_desc=sdesc,city=city,uom = uom,qty = qty,c_regis_id=request.session['comp_id'],minimum_price = mprice,scrap_img=request.FILES['txtimg'],last_bid_date = datetime.now()+timedelta(days=10))
        scrap.save()

        messages.info(request, 'Scrap Upload Successfully')
        return redirect('company_upload_scrap')
    else:
        return render(request, 'company_upload_scrap.html')



def company_view_scrap(request):
    Scraps = Scrap_detail.objects.filter(c_regis_id=request.session['comp_id'])
    return render(request, 'company_view_scrap.html', {'Scraps': Scraps})


def admin_view_scrap(request):
    Scraps = Scrap_detail.objects.filter(status=0)
    Comp = User_Registration.objects.filter(user_type='COMPANY')
    return render(request, 'admin_view_scrap.html', {'Scraps': Scraps,'Comp':Comp})


def verifyscrap(request,id):
    scrap = Scrap_detail.objects.get(id=id)
    scrap.status=1

    scrap.save()
    messages.info(request, 'Scrap Verified Successfully')
    return redirect('admin_view_scrap')




def notverifyscrap(request,id):
    scrap = Scrap_detail.objects.get(id=id)
    scrap.status = 2

    scrap.save()
    messages.info(request, 'Scrap Not Verified Successfully')
    return redirect('admin_view_scrap')


def dealer_view_scrap(request):
    myDate = datetime.now()
    formatedDate = myDate.strftime("%Y-%m-%d")
    Scraps = Scrap_detail.objects.filter(last_bid_date__gte = formatedDate,status=1)
    return render(request, 'dealer_view_scrap.html', {'Scraps': Scraps})

def dealer_view_scrap_detail(request,id):
    if request.method == 'POST':
        bamt = int(request.POST['txtbamt'])
        scrap = Scrap_detail.objects.get(id=id)
        if scrap.minimum_price > bamt:
            Scraps = Scrap_detail.objects.filter(id=id)
            messages.info(request, 'Bid Amount is Not Less than Minimum Amount')
            return render(request, 'dealer_view_scrap_detail.html', {'Scraps': Scraps})
        else:
            bid = Bid_detail(d_regis_id=request.session['dealer_id'],scrap_id=id,bid_amount=bamt)
            bid.save()
            messages.info(request, 'Bid Done Successfully')
            return redirect('dealer_view_scrap')
    else:
        Scraps = Scrap_detail.objects.filter(id=id)
        Bid = Bid_detail.objects.filter(scrap_id=id,d_regis_id=request.session['dealer_id'])
        return render(request, 'dealer_view_scrap_detail.html', {'Scraps': Scraps,'Bid':Bid})