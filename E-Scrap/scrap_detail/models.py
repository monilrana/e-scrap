from django.db import models
from datetime import datetime

# Create your models here.

class Scrap_detail(models.Model):
    scrap_name = models.CharField(max_length=255)
    scrap_desc = models.TextField()
    city = models.CharField(max_length=255)
    c_regis_id=models.IntegerField()
    uom= models.CharField(max_length=20)
    qty = models.IntegerField()
    minimum_price = models.IntegerField()
    scrap_upload_date = models.DateField(auto_now_add=True)
    last_bid_date = models.DateField()
    scrap_img = models.ImageField(upload_to='pics')
    status = models.IntegerField(default=0)


