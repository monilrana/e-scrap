from django.urls import path


from . import views

urlpatterns = [
    path('company_upload_scrap',views.company_upload_scrap, name="company_upload_scrap"),
    path('company_view_scrap',views.company_view_scrap, name="company_view_scrap"),
    path('admin_view_scrap',views.admin_view_scrap, name="admin_view_scrap"),
    path('verifyscrap/<int:id>', views.verifyscrap),
    path('notverifyscrap/<int:id>', views.notverifyscrap),
    path('dealer_view_scrap',views.dealer_view_scrap, name="dealer_view_scrap"),
    path('dealer_view_scrap_detail/<int:id>',views.dealer_view_scrap_detail, name="dealer_view_scrap"),
]