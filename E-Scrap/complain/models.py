from django.db import models

# Create your models here.


class Complain_Detail(models.Model):
    complain_date = models.DateField(auto_now_add=True)
    complainer_regis_id = models.IntegerField()
    complain_against_name= models.CharField(max_length=255)
    complain_reason=models.TextField()
    status = models.IntegerField(default=0)



