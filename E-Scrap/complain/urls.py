from django.urls import path


from . import views

urlpatterns = [
    path('dealer_complain',views.dealer_complain),
    path('dealer_view_complain',views.dealer_view_complain),
    path('company_complain',views.company_complain),
    path('company_view_complain',views.company_view_complain),
    path('admin_view_complain',views.admin_view_complain),
    path('admin_update_comp_status/<int:id>',views.admin_update_comp_status),
]