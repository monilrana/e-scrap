from django.shortcuts import render, redirect
from .models import Complain_Detail
from accounts.models import User_Registration
# Create your views here.
from django.contrib import messages

def dealer_complain(request):
    if request.method == 'POST':
        aname = request.POST['txtname']
        creason = request.POST['txtres']
        regisid = request.session['dealer_id']
        cd = Complain_Detail(complainer_regis_id=regisid, complain_against_name=aname, complain_reason=creason)
        cd.save()
        messages.info(request, 'Complain Registered Succesfully')
        return redirect('/complain/dealer_complain')
    else:
        return render(request,"dealer_complain.html")



def dealer_view_complain(request):
   comp = Complain_Detail.objects.filter(complainer_regis_id=request.session['dealer_id'])
   return render(request,"dealer_view_complain.html",{'comp':comp})



def company_complain(request):
    if request.method == 'POST':
        aname = request.POST['txtname']
        creason = request.POST['txtres']
        regisid = request.session['comp_id']
        cd = Complain_Detail(complainer_regis_id=regisid, complain_against_name=aname, complain_reason=creason)
        cd.save()

        messages.info(request, 'Complain Registered Succesfully')
        return redirect('/complain/company_view_complain')
    else:
        return render(request,"company_complain.html")



def company_view_complain(request):
   comp = Complain_Detail.objects.filter(complainer_regis_id=request.session['comp_id'])
   return render(request,"company_view_complain.html",{'comp':comp})



def admin_view_complain(request):
    user = User_Registration.objects.filter()
    comp = Complain_Detail.objects.filter()
    return render(request,"admin_view_complain.html",{'comp':comp,'user':user})



def admin_update_comp_status(request,id):
    if request.method == 'POST':
        status=request.POST['selstatus']
        comp = Complain_Detail.objects.get(id=id)
        comp.status=status
        comp.save()
        return redirect('/complain/admin_view_complain')
    else:
        return render(request,"admin_update_comp_status.html",{'id':id})