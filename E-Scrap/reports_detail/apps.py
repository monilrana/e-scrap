from django.apps import AppConfig


class ReportsDetailConfig(AppConfig):
    name = 'reports_detail'
