from django.shortcuts import render,redirect
from scrap_detail.models import Scrap_detail
from bid_detail.models import Bid_detail,Winner_detail
from accounts.models import User_Registration
from datetime import datetime


# Create your views here.
def admin_view_company_detail_report(request):
    try:
        users = User_Registration.objects.filter(user_type='COMPANY',status='1')
        return render(request, 'admin_view_company_detail_report.html', {'users': users})
    except:
        return render(request, 'admin_view_company_detail_report.html')



def admin_view_companywise_scrap_report(request,id):
    try:
        Scraps = Scrap_detail.objects.filter(c_regis_id=id)
        return render(request, 'admin_view_companywise_scrap_report.html', {'Scraps': Scraps})
    except:
        return render(request, 'admin_view_companywise_scrap_report.html')


def admin_view_scrapwise_bidding_report(request,id):
    try:
        Scraps = Scrap_detail.objects.filter(id=id)
        Bid = Bid_detail.objects.filter(scrap_id=id)
        user = User_Registration.objects.filter()
        return render(request, 'admin_view_scrapwise_bidding_report.html', {'Scraps': Scraps, 'Bid': Bid, 'user': user})
    except:
        return render(request, 'admin_view_scrapwise_bidding_report.html')



def admin_view_scrapwise_winner_report(request,id):
    try:
        Scraps = Scrap_detail.objects.filter(id=id)
        Bid = Bid_detail.objects.filter(scrap_id=id).order_by('-bid_amount')
        user = User_Registration.objects.filter()
        winner = Winner_detail.objects.filter(scrap_id=id)
        return render(request, 'admin_view_scrapwise_winner_report.html',
                      {'Scraps': Scraps, 'Bid': Bid, 'user': user, 'winner': winner})
    except:
        return render(request, 'admin_view_scrapwise_winner_report.html')




def admin_view_dealer_detail_report(request):
    try:
        users = User_Registration.objects.filter(user_type='DEALER',status='1')
        return render(request, 'admin_view_dealer_detail_report.html', {'users': users})
    except:
        return render(request, 'admin_view_dealer_detail_report.html')



def admin_view_dealerwise_bidding_report(request,id):
    try:
        Scraps = Scrap_detail.objects.filter()
        Bid = Bid_detail.objects.filter(d_regis_id=id)
        user = User_Registration.objects.filter()
        return render(request, 'admin_view_dealerwise_bidding_report.html', {'Scraps': Scraps, 'Bid': Bid, 'user': user})
    except:
        return render(request, 'admin_view_dealerwise_bidding_report.html')




def admin_view_datewise_scrap_upload_report(request):
    myDate = datetime.now()
    # Give a format to the date
    # Displays something like: Aug. 27, 2017, 2:57 p.m.
    formatedDate = myDate.strftime("%Y-%m-%d")
    if request.method == 'POST':
        fdate = request.POST['txtfdate']
        tdate = request.POST['txttdate']
        try:
            Scraps = Scrap_detail.objects.filter(scrap_upload_date__gte=fdate, scrap_upload_date__lte=tdate)
            user = User_Registration.objects.filter()
            return render(request, 'admin_view_datewise_scrap_upload_report.html',
                          {'Scraps': Scraps, 'user': user, 'fdate': fdate, 'tdate': tdate})
        except:
            return render(request, 'admin_view_datewise_scrap_upload_report.html')
    else:
        return render(request, 'admin_view_datewise_scrap_upload_report.html', {'fdate': formatedDate, 'tdate': formatedDate})



def admin_view_scrap_detail_report(request):
    try:
        Scraps = Scrap_detail.objects.filter(status=3)
        user = User_Registration.objects.filter()
        return render(request, 'admin_view_scrap_detail_report.html',
                      {'Scraps': Scraps, 'user': user})
    except:
        return render(request, 'admin_view_scrap_detail_report.html')