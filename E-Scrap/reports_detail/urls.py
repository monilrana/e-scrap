from django.urls import path

from . import views

urlpatterns = [
    path('admin_view_company_detail_report', views.admin_view_company_detail_report, name="admin_view_company_detail_report"),
    path('admin_view_companywise_scrap_report/<int:id>', views.admin_view_companywise_scrap_report),
    path('admin_view_scrapwise_bidding_report/<int:id>', views.admin_view_scrapwise_bidding_report),
    path('admin_view_scrapwise_winner_report/<int:id>', views.admin_view_scrapwise_winner_report),
    path('admin_view_dealer_detail_report', views.admin_view_dealer_detail_report),
    path('admin_view_dealerwise_bidding_report/<int:id>', views.admin_view_dealerwise_bidding_report),
    path('admin_view_datewise_scrap_upload_report', views.admin_view_datewise_scrap_upload_report),
    path('admin_view_scrap_detail_report', views.admin_view_scrap_detail_report),
    #path('admin_view_max_product_sold_report', views.admin_view_max_product_sold_report),
]