from django.shortcuts import render, redirect
from bid_detail.models import Bid_detail, Winner_detail
from scrap_detail.models import Scrap_detail
from datetime import date
from django.contrib import messages
from accounts.models import User_Registration

# Create your views here.

def company_view_bid(request,id):
    Scraps = Scrap_detail.objects.filter(id=id)
    Bid = Bid_detail.objects.filter(scrap_id=id)
    user = User_Registration.objects.filter()
    return render(request, 'company_view_bid.html', {'Scraps': Scraps, 'Bid': Bid,'user':user})


def dealer_view_bid(request):
    dealer_id = request.session['dealer_id']
    Scraps = Scrap_detail.objects.filter()
    Bid = Bid_detail.objects.filter(d_regis_id=dealer_id)
    user = User_Registration.objects.filter()
    return render(request, 'dealer_view_bid.html', {'Scraps': Scraps, 'Bid': Bid,'user':user})



def company_view_scrap_for_selectwinner(request):
    Scraps = Scrap_detail.objects.filter(c_regis_id=request.session['comp_id'],last_bid_date__lte = date.today(),status=1)
    return render(request, 'company_view_scrap_for_selectwinner.html', {'Scraps': Scraps})


def company_select_winner(request,id):
    Scraps = Scrap_detail.objects.filter(id=id)
    Bid = Bid_detail.objects.filter(scrap_id=id).order_by('-bid_amount')
    user = User_Registration.objects.filter()
    return render(request, 'company_select_winner.html', {'Scraps': Scraps, 'Bid': Bid,'user':user})



def company_selected_winner(request,id):
    bid = Bid_detail.objects.get(id=id)
    winner = Winner_detail(d_regis_id=bid.d_regis_id, scrap_id=bid.scrap_id,bid_id=bid.id, bid_amount=bid.bid_amount)
    winner.save()
    scrap = Scrap_detail.objects.get(id=bid.scrap_id)
    scrap.status=3
    scrap.save()
    messages.info(request, 'Bid Done Successfully')
    return redirect('/bid_detail/company_view_scrap_for_viewwinner')
    #return render(request, 'company_select_winner.html', {'Scraps': Scraps, 'Bid': Bid,'user':user})


def company_view_scrap_for_viewwinner(request):
    Scraps = Scrap_detail.objects.filter(c_regis_id=request.session['comp_id'],status=3)
    return render(request, 'company_view_scrap_for_viewwinner.html', {'Scraps': Scraps})


def company_view_winner_detail(request,id):
    Scraps = Scrap_detail.objects.filter(id=id)
    Bid = Bid_detail.objects.filter(scrap_id=id).order_by('-bid_amount')
    user = User_Registration.objects.filter()
    winner = Winner_detail.objects.filter(scrap_id=id)
    return render(request, 'company_view_winner_detail.html', {'Scraps': Scraps, 'Bid': Bid,'user':user,'winner':winner})



def dealer_view_scrap_winner(request):
    dealer_id = request.session['dealer_id']
    Scraps = Scrap_detail.objects.filter(status=3)
    Bid = Bid_detail.objects.filter(d_regis_id=dealer_id,scrap_id__in=Scrap_detail.objects.filter(status=3).values_list('id', flat=True))
    user = User_Registration.objects.filter()
    winner = Winner_detail.objects.filter(id__in=Bid_detail.objects.filter().values_list('id', flat=True))
    return render(request, 'dealer_view_scrap_winner.html', {'Scraps': Scraps, 'Bid': Bid,'user':user,'winner':winner})