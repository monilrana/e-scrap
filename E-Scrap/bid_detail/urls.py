from django.urls import path


from . import views

urlpatterns = [
    path('company_view_bid/<int:id>',views.company_view_bid),
    path('dealer_view_bid',views.dealer_view_bid),
    path('company_view_scrap_for_selectwinner',views.company_view_scrap_for_selectwinner),
    path('company_select_winner/<int:id>',views.company_select_winner),
    path('company_selected_winner/<int:id>',views.company_selected_winner),
    path('company_view_scrap_for_viewwinner',views.company_view_scrap_for_viewwinner),
    path('company_view_winner_detail/<int:id>',views.company_view_winner_detail),
    path('dealer_view_scrap_winner',views.dealer_view_scrap_winner),
]