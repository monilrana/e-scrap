from django.db import models
from datetime import datetime

# Create your models here.

class Bid_detail(models.Model):
    bid_date = models.DateField(auto_now_add=True)
    d_regis_id = models.IntegerField()
    scrap_id = models.IntegerField()
    bid_amount = models.IntegerField()



class Winner_detail(models.Model):
    winner_date = models.DateField(auto_now_add=True)
    d_regis_id = models.IntegerField()
    scrap_id = models.IntegerField()
    bid_id = models.IntegerField()
    bid_amount = models.IntegerField()