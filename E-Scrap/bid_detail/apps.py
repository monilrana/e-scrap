from django.apps import AppConfig


class BidDetailConfig(AppConfig):
    name = 'bid_detail'
