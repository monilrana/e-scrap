# Generated by Django 2.1 on 2019-08-09 12:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User_Registration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('address', models.TextField()),
                ('city', models.CharField(max_length=255)),
                ('mobile_no', models.CharField(max_length=10)),
                ('email_id', models.CharField(max_length=255)),
                ('pwd', models.CharField(max_length=255)),
                ('user_type', models.CharField(max_length=50)),
                ('status', models.IntegerField(default=0)),
                ('license_img', models.ImageField(max_length=2083, upload_to='')),
            ],
        ),
    ]
