from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth.models import User,auth
from django.contrib import messages
from django.http import HttpResponse
from .models import User_Registration
#from accounts.forms import User_RegistrationForm,User_Loginform
# Create your views here.
from django.db.models import Q


def register(request):
    if request.method == 'POST' and request.FILES['txtfile']:
        name = request.POST['txtname']
        add = request.POST['txtadd']
        city = request.POST['txtcity']
        mno = request.POST['txtmno']
        email = request.POST['txtemail']
        pwd = request.POST['txtpwd']
        utype = request.POST['seluser']
        user = User_Registration()
        try:
            m = User_Registration.objects.get(email_id=email)
            messages.info(request, 'Email Id Already Exists')
            return redirect('/accounts/register')
        except user.DoesNotExist:
            cust1 = User_Registration(name=name, address=add, city=city, mobile_no=mno, email_id=email, pwd=pwd,user_type=utype,status=0,license_img=request.FILES['txtfile'])
            cust1.save()
            messages.info(request, 'Registration Done')
            return redirect('/accounts/login')
    else:
       return render(request, 'register.html')


def login(request):
    if request.method == 'POST':

        email = request.POST['txtemail']
        pwd1 = request.POST['txtpwd']
        user = auth.authenticate(username=email, password=pwd1)
        if user is not None:
            messages.info(request, 'Admin Login Succesfull')
            #user = User_Registration.objects.filter(user_type='DEALER')
            return redirect('verify_dealer')
        else:
            user = User_Registration()
            try:
                m = User_Registration.objects.get(email_id=email,pwd=pwd1)
                #if m.pwd == pwd1:
                if m.user_type == 'DEALER':
                    if m.status==0:
                        messages.info(request, 'Yet To Verified')
                        return redirect('/accounts/login')
                    elif m.status==1:
                        request.session['dealer_id'] = m.id
                        messages.info(request, 'Dealer Login Successfull')
                        return redirect('/scrap_detail/dealer_view_scrap')
                    elif m.status==2:
                        messages.info(request, 'You Are Not Verified User Contact Our Admin')
                        return redirect('/contact')
                    elif m.status == 3:
                        messages.info(request, 'You Are Blocked By Admin Contact Our Admin')
                        return redirect('/contact')
                else:
                    if m.status==0:
                        messages.info(request, 'Yet To Verified')
                        return redirect('/accounts/login')
                    elif m.status==1:
                        request.session['comp_id'] = m.id
                        messages.info(request, 'Company Login Successfull')
                        return redirect('/scrap_detail/company_upload_scrap')
                    elif m.status==2:
                        messages.info(request, 'You Are Not Verified User Contact Our Admin')
                        return redirect('/contact')
                    elif m.status == 3:
                        messages.info(request, 'You Are Blocked By Admin Contact Our Admin')
                        return redirect('/contact')


            except user.DoesNotExist:
                messages.info(request, 'Invalid Credentials')
                return redirect('/accounts/login')
    else:
        return render(request,'login.html')




def verify_dealer(request):
    users = User_Registration.objects.filter(user_type='DEALER',status=0)
    return render(request, 'verify_dealer.html', {'users': users})


def verifydealer(request,id):
    dealer = User_Registration.objects.get(id=id)
    dealer.status=1
    if dealer.user_type == 'DEALER':
        dealer.save()
        return redirect('verify_dealer')
    else:
        dealer.save()
        return redirect('verify_company')


def notverifyd(request,id):
    dealer = User_Registration.objects.get(id=id)

    dealer.status = 2
    if dealer.user_type == 'DEALER':
        dealer.save()
        return redirect('verify_dealer')
    else:
        dealer.save()
        return redirect('verify_company')

def verify_company(request):
    users = User_Registration.objects.filter(user_type='COMPANY',status=0)
    return render(request, 'verify_company.html', {'users': users})

def logout(request):
    try:
        del request.session['admin_id']
    except KeyError:
        pass
    return redirect("/")



def admin_block_user(request):
    user = User_Registration.objects.filter(Q(status=1)|Q(status=3))
    return render(request,"admin_block_user.html",{'users':user})



def block_user(request,id):
    user = User_Registration.objects.get(id=id)
    user.status=3
    user.save()
    messages.info(request, 'User Blocked Successfully')
    return redirect('/accounts/admin_block_user')


def unblock_user(request,id):
    user = User_Registration.objects.get(id=id)
    user.status=1
    user.save()
    messages.info(request, 'User Unblocked Successfully')
    return redirect('/accounts/admin_block_user')