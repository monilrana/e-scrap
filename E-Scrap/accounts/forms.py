from django import forms
from accounts.models import User_Registration


USER_CHOICE= [
    ('DEALER', 'DEALER'),
    ('COMPANY', 'COMPANY'),
]

class User_RegistrationForm(forms.ModelForm):
    name = forms.CharField(max_length=100,
                                widget=forms.TextInput
                                (attrs={'placeholder': 'Enter your Name'}))
    address = forms.CharField(max_length=250,
                               widget=forms.Textarea
                               (attrs={'placeholder': 'Enter your Address'}))
    city = forms.CharField(max_length=100,
                           widget=forms.TextInput
                           (attrs={'placeholder': 'Enter your City'}))
    mobile_no = forms.CharField(max_length=100,
                                  widget=forms.TextInput
                                  (attrs={'placeholder': 'Enter Your Mobile No'}))
    email_id = forms.CharField(max_length=100,
                               widget=forms.TextInput
                               (attrs={'placeholder': 'Enter your Email'}))
    pwd = forms.CharField(max_length=100,
                               widget=forms.PasswordInput
                               (attrs={'placeholder': 'Enter your Password'}))
    user_type = forms.CharField(label='Select User Type', widget=forms.Select(choices=USER_CHOICE))

    class Meta:
        model = User_Registration
        fields = ("name","address","city","mobile_no","email_id","pwd","user_type","license_img")


class User_Loginform(forms.ModelForm):
    email_id = forms.CharField(max_length=100,
                               widget=forms.TextInput
                               (attrs={'placeholder': 'Enter your Email'}))
    pwd = forms.CharField(max_length=100,
                               widget=forms.PasswordInput
                               (attrs={'placeholder': 'Enter your Password'}))
    class Meta:
        model = User_Registration
        fields = ("email_id","pwd")


