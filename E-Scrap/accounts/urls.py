from django.urls import path


from . import views

urlpatterns = [
    path('register',views.register, name="register"),
    path('login',views.login, name="login"),
    path('verify_dealer',views.verify_dealer, name="verify_dealer"),
    path('verify_company',views.verify_company, name="verify_company"),
    path('verifydealer/<int:id>', views.verifydealer),
    path('notverifyd/<int:id>', views.notverifyd),
    path('logout',views.logout,name="logout"),
    path('admin_block_user',views.admin_block_user, name="admin_block_user"),
    path('block_user/<int:id>', views.block_user),
    path('unblock_user/<int:id>', views.unblock_user),
]