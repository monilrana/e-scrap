from django.db import models

# Create your models here.


class User_Registration(models.Model):
    name = models.CharField(max_length=255)
    address = models.TextField()
    city = models.CharField(max_length=255)
    mobile_no=models.CharField(max_length=10)
    email_id= models.CharField(max_length=255)
    pwd = models.CharField(max_length=255)
    user_type = models.CharField(max_length=50)
    status = models.IntegerField(default=0)
    license_img = models.ImageField(upload_to='pics')



